package movies.importer;

public class Movie {
	private String releaseYear;
	private String name;
	private String runtime;
	private String source;


public Movie(String releaseYear,String name,String runtime,String source) {
	this. releaseYear= releaseYear;
	this.name=name;
	this.runtime=runtime;
	this.source=source;
}

public String getReleaseYear() {
	return this. releaseYear;
}
public String getName() {
	return this. name;
}
public String getRuntime() {
	return this.runtime;
}
public String getSource() {
	return this.source;
}
public void setName(String name) {
	this.name=name;
}

public void setRuntime(String runtime) {
	this.runtime=runtime;
}
public void setSource(String source) {
	this.source=source;}

public String toString() {
	String s=this. releaseYear+"	"+this.name+"	"+this.runtime+"	"+this.source;
	
	return s;
}
@Override
public boolean equals(Object o) {
	if (this.name.equals(((Movie)o).name) && this.releaseYear.equals(((Movie)o).releaseYear) && Integer.parseInt(this.runtime)-Integer.parseInt(((Movie)o).runtime)<=5  && Integer.parseInt(this.runtime)-Integer.parseInt(((Movie)o).runtime)>=-5)  {return true;}
	else
		return false;
}

}

