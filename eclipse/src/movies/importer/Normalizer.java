package movies.importer;

import java.util.ArrayList;

public class Normalizer extends Processor {
	
	public 	Normalizer( String srcDir, String outputDir) {
		super(srcDir,outputDir,false);}
		public  ArrayList<String> process(ArrayList<String> input){
			ArrayList<String> normalized = new ArrayList<String>();
			for(int i=0;i<input.size();i++) {
				String x=input.get(i);
				String []line=x.split("\\t");
				Movie xy=new Movie (line[0],line[1],line[2],"kaggle");
				String title=xy.getName();
				String Ltitle=title.toLowerCase();
				xy.setName(Ltitle);
				String runtime=xy.getRuntime();
				String cutruntime=runtime.substring(0,runtime.indexOf(" "));
				xy.setRuntime(cutruntime);
				String s=xy.toString();
				normalized.add(s);
			}
			
return normalized;
}
}
