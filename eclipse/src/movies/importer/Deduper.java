	package movies.importer;
	import java.util.ArrayList;
	
	public class Deduper extends Processor{
		
		
		public Deduper (String sourceDir, String outputDir) {	
			super( sourceDir, outputDir, false);}
		
		
		public  ArrayList<String> process(ArrayList<String> input){
			ArrayList<String> noDuplicate = new ArrayList<String>();
			ArrayList<Movie> noDupObj = new ArrayList<Movie>();
			Movie merged=new Movie("","","","");
			Movie z=new Movie("","","","");
			for (int i=0; i<input.size();i++) {
			String x=input.get(i);
				String[]y=x.split("\\t");
				 z=new Movie(y[0],y[1],y[2],y[3]);
				 
				/* for (int j =0;j<noDupObj.size();i++) {
					 if (noDupObj.get(j).equals(z)){
						 if (noDupObj.get(j).getSource()==z.getSource())
							{ merged=new Movie (noDupObj.get(j).getReleaseYear(),noDupObj.get(j).getName(),noDupObj.get(j).getRuntime(),noDupObj.get(j).getSource());}
								else
									merged=new Movie (noDupObj.get(j).getReleaseYear(),noDupObj.get(j).getName(),noDupObj.get(j).getRuntime(),"kaggle;imdb");
							
							if(Integer.parseInt(noDupObj.get(j).getRuntime())>Integer.parseInt(z.getRuntime()))
									merged.setRuntime(z.getRuntime());
							else
								
								merged.setRuntime(noDupObj.get(j).getRuntime());	
							noDupObj.set(j, merged);
							break;
					 }
						 
				 }
				 
				 noDupObj.add(z);}
					 
			for (int f=0; f<noDupObj.size();f++) {
				String s=noDupObj.get(f).toString();
				noDuplicate.add(s);
			}
			return noDuplicate;
				 }
	}*/
				 
				 
				if (!noDupObj.contains(z)) 
					
					noDupObj.add(z);
				else
				{int j=noDupObj.indexOf(z);
					
				
						
						if ((noDupObj.get(j)).getSource().equals(z.getSource()))
					{ merged=new Movie (noDupObj.get(j).getReleaseYear(),noDupObj.get(j).getName(),noDupObj.get(j).getRuntime(),noDupObj.get(j).getSource());}
						else
							merged=new Movie (noDupObj.get(j).getReleaseYear(),noDupObj.get(j).getName(),noDupObj.get(j).getRuntime(),"kaggle;imdb");
					
					if(Integer.parseInt(noDupObj.get(j).getRuntime())>Integer.parseInt(z.getRuntime()))
							merged.setRuntime(z.getRuntime());
					else
						
						merged.setRuntime(noDupObj.get(j).getRuntime());	
							
							
					noDupObj.set(j, merged);}}
						
			for (int f=0; f<noDupObj.size();f++) {
				String s=noDupObj.get(f).toString();
				noDuplicate.add(s);
			}
		
			
			return noDuplicate;
			}}
