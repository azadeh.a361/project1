package movies.tests;
import movies.importer.Normalizer;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;



class NormalizerTest {

	@Test
	public void tester() {
		ArrayList <String> test=new ArrayList<String>();
		test.add("2016	The Masked Saint	111 minutes	Kaggle"	);
		ArrayList <String> testback=new ArrayList<String>();
		testback.add("2016	the masked saint	111	kaggle");
		Normalizer o=new Normalizer(" srcDir", " outputDir");
		assertEquals(testback,o.process(test));
		
	}

}
